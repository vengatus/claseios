//
//  TabBarController.swift
//  SizeClasses
//
//  Created by Juan Erazo on 22/5/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        let items = tabBar.items
//        items?[0].badgeValue = "10"
//        items?[0].badgeColor = .black
//        
//        items?[1].badgeValue = "2"
//        items?[1].badgeColor = .green
//        
//        items?[2].badgeValue = "3"
//        items?[2].badgeColor = UIColor(red: 0/255.0, green: 127/255.0, blue: 255/255.0, alpha: 1)
//                //alpha es la transparencia
//        items?[2].title = "Tre'"
//        

        

        
        items?[0].badgeValue = "3"
        items?[0].badgeColor = UIColor(red: 0/255.0, green: 127/255.0, blue: 255/255.0, alpha: 1)
        //alpha es la transparencia
        items?[0].title = "StoryBoard1"
        items?[0].image = #imageLiteral(resourceName: "animal-footprint")
        
        items?[1].badgeValue = "3"
        items?[1].badgeColor = UIColor(red: 32/255.0, green: 143/255.0, blue: 200/255.0, alpha: 1)
        //alpha es la transparencia
        items?[1].title = "StoryBoard2"
        items?[1].image = #imageLiteral(resourceName: "antique-vase")
        
        tabBar.tintColor = UIColor(red: 23/255.0, green: 234/255.0, blue: 122/255.0, alpha: 1)


    }
    
    

}
