//
//  PokemonViewController.swift
//  SizeClasses
//
//  Created by Juan Erazo on 8/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class PokemonViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    let backend = BackEndManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    //    MARK: - Seccion de TableView
    //    Con estos metodos sera suficiente para crear la tabla
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1; //solo se trabajara con 1 seccion
    }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 20;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if let pkImage = pokeImages[indexPath.row] { //Si es que existe esa imagen
            cell.imageView?.image = pkImage // la igualo con su valor
        } else {
            backend.downloadImages(indexPath.row)
        }
        return cell
//        return tableView.dequeueReusableCell(withIdentifier: "celdaPaises")!
    }
}
