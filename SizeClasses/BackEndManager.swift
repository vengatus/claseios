//
//  BackEndManager.swift
//  SizeClasses
//
//  Created by Juan Erazo on 8/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import Foundation
import AlamofireImage
import Alamofire


class BackEndManager {
    
    func downloadImages(_ id:Int){
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        Alamofire.request(url).responseImage { response in
            
            guard let image = response.result.value else {
                return
            }
            
            pokeImages[id] = image
           
        }
    }
    

}
