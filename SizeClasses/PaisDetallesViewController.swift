//
//  PaisDetallesViewController.swift
//  SizeClasses
//
//  Created by Juan Erazo on 29/5/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class PaisDetallesViewController: UIViewController {

    @IBOutlet weak var detalleLabel: UILabel!
    
    var indexPath:IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        detalleLabel.text = paisesAmerica[indexPath!.row]
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
