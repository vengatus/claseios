//
//  TablaViewController.swift
//  SizeClasses
//
//  Created by Juan Erazo on 25/5/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class TablaViewController: UIViewController,
//    Antes se debia arrastar el TableView al icono amarillo que representa este viewcontroller
//  Se deben utilizar estas clases para utilizar los TableViews:   (dara un error en el que debeos implementar los metodos)
    UITableViewDataSource, UITableViewDelegate {
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    MARK: - Seccion de TableView
//    Con estos metodos sera suficiente para crear la tabla
    func numberOfSections(in tableView: UITableView) -> Int {
        return continentes.count; //solo se trabajara con 1 seccion
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        aqui se regresaran el numero de elementos que habra por secciones (En Sudamerica habra 10 paises) toca hacer un switch al parametro section
        
        
        switch section {
        case 0:
            return paisesAmerica.count
        case 1:
            return paisesAsia.count
        case 2:
            return paisesEuropa.count
        case 3:
            return paisesAntartida.count
        case 4:
            return paisesOceania.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        Se debe retornar una UITableViewCell
//        Para este caso, se va a customizar la celda
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaPaises") as! PaisesTableViewCell
        
        switch indexPath.section { //hago un switch de a cuerdo a la seccion
        case 0:
            cell.nombreLabel.text = paisesAmerica[indexPath.row]
//            cell.textLabel?.text = paisesAmerica[indexPath.row]
//            cell.paisImageView.image = banderas[indexPath.row]
            guard let bandera = banderas[paisesAmerica[indexPath.row]] else {
                cell.paisImageView.image = #imageLiteral(resourceName: "random")
                return cell
            }
            cell.paisImageView.image = bandera
            
        case 1:
            cell.nombreLabel.text = paisesAsia[indexPath.row]
            guard let bandera = banderas[paisesAsia [indexPath.row]] else {
                cell.paisImageView.image = #imageLiteral(resourceName: "random")
                return cell
            }
            cell.paisImageView.image = bandera
            
        case 2:
            cell.nombreLabel.text = paisesEuropa[indexPath.row]
            guard let bandera = banderas[paisesEuropa[indexPath.row]] else {
                cell.paisImageView.image = #imageLiteral(resourceName: "random")
                return cell
            }
            cell.paisImageView.image = bandera
        case 3:
            cell.nombreLabel.text = paisesAntartida[indexPath.row]
            guard let bandera = banderas[paisesAntartida[indexPath.row]] else {
                cell.paisImageView.image = #imageLiteral(resourceName: "random")
                return cell
            }
            cell.paisImageView.image = bandera
        default:
            cell.nombreLabel.text = paisesOceania[indexPath.row]
            guard let bandera = banderas[paisesOceania[indexPath.row]] else {
                cell.paisImageView.image = #imageLiteral(resourceName: "random")
                return cell
            }
            cell.paisImageView.image = bandera
//        default:
//            cell.textLabel?.text = "vacio jiji"
            
        }
        return cell
    }
    
    var celdaSeleccionada = IndexPath()
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destino = segue.destination as! PaisDetallesViewController
        destino.indexPath = celdaSeleccionada
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        celdaSeleccionada = indexPath
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return continentes[section]
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
//  Se tiene que designar el delegado:
    


}
