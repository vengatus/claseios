//
//  PaisesTableViewCell.swift
//  SizeClasses
//
//  Created by Juan Erazo on 29/5/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class PaisesTableViewCell: UITableViewCell {
    @IBOutlet weak var paisImageView: UIImageView!
    @IBOutlet weak var nombreLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

     }

}
